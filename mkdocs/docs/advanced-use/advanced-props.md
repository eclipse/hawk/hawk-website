When querying through EOL, we can access several extra properties on any model element:

* `eAllContents`: collection with all the model elements directly or indirectly contained within this one.
* `eContainer`: returns the model element that contains this one, or `null` if it does not have a container.
* `eContents`: collection with all the model elements directly contained within this one.
* `hawkFile`: string with the repository paths of the files that this model element belongs to, separated by ";".
* `hawkFiles`: collection with the repository paths of all the files that this model element belongs to.
* `hawkIn`: collection with all the model elements accessible through incoming references.
* `hawkInEdges`: collection with all the incoming references (see their attributes below).
* `hawkNodeId`: (since 2.3.0) raw ID of the underlying graph node.
* `hawkOut`: collection with all the model elements accessible through outgoing references.
* `hawkOutEdges`: collection with all the outgoing references (see their attributes below).
* `hawkProxies`: collection with all the proxy reference lists (see their properties below).
* `hawkRepo`: string with the URLs of the repositories that this model element belongs to, separated by ";".
* `hawkRepos`: collection with all the repositories that this model element belongs to.
* `hawkURIFragment`: URI fragment of the model element within its file.

There is also the `isContainedWithin(repo, path)` method for checking if an element is directly or indirectly contained within a certain file.

## References

References are wrapped into entities of their own, with the following attributes:

* `edge`: raw edge, without wrapping.
* `type` / `name`: name of the reference.
* `source` / `startNode`: source of the reference.
* `target` / `endNode`: target of the reference.

## Proxy reference lists

A proxy reference list represents all the unresolved links from a node to the elements in a certain file.
These links may be unresolved as the file may be missing, or the specific elements may not be in the file.
Proxy reference lists have the following fields:

* `sourceNodeID`: unique ID of the source model element node for these proxy references.
* `targetFile`: returns an object which refers to the target file. This object has several fields:
    * `repositoryURL`: string with URL of the repository that should have this file.
    * `filePath`: string with path within the repository for the file.
* `references`: returns a collection with each of the proxy references to the missing file. Each reference has the following fields:
    * `edgeLabel`: name of the proxy reference.
    * `isContainment`: `true` if and only if the proxy reference is a containment reference (the target is contained within the source).
    * `isContainer`: `true` if and only if the proxy reference is a container reference (the source is contained within the target).
    * `target`: object which refers to the target of the proxy reference. This object has several fields:
        * `repositoryURL`: string with URL of the repository that should have this file.
        * `filePath`: string with path within the repository for the file.
        * `fragment`: string with the fragment that identifies the model element within the file.
        * `isFragmentBased`: `true` if and only if the proxy reference is purely fragment-based (file path is irrelevant). This can be the case for some modelling technologies (e.g. Modelio).
