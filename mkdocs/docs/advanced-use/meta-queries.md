Hawk extends the regular EOL facilities to be able to query the metamodels registered within the instance:

* `Model.files` lists all the files indexed by Hawk (may be limited through the context).
* `Model.getFileOf(obj)` retrieves the first file containing the object `obj`.
* `Model.getFilesOf(obj)` retrieves all the files containing the object `obj`.
* `Model.getProxies(repositoryPrefix)` lists all the proxy reference lists for files in repositories matching the specified prefix.
* `Model.getTypeOf(obj)` retrieves the type of the object `obj`.
* `Model.metamodels` lists all the metamodels registered in Hawk (`EPackage` instances for EMF).
* `Model.proxies` lists all the proxy reference lists present in the graph. Each proxy reference list is a collection of the unresolved references from a model element node to the elements of a particular file. For details, please consult the [advanced properties page](advanced-props.md#proxy-reference-lists).
* `Model.types` lists all the types registered in Hawk (`EClass` instances for EMF).

## Metamodels

For a metamodel `mm`, these attributes are available:

* `mm.dependencies` lists the metamodels this metamodel depends on (usually at least the Ecore metamodel for EMF-based metamodels).
* `mm.metamodelType` is the type of metamodel that was registered.
* `mm.node` returns the underlying `IGraphNode`.
* `mm.resource` retrieves the original string representation for this metamodel (the original `.ecore` file for EMF).
* `mm.types` lists the types defined in this metamodel.
* `mm.uri` is the namespace URI of the metamodel.

## Types

For a type `t`, these attributes are available:

* `t.allSubtypes` (since 2.3.0) lists all direct and indirect subtypes of the type.
* `t.allSupertypes` (since 2.3.0) lists all direct and indirect supertypes of the type.
* `t.all` retrieves all instances of that type efficiently (includes subtypes).
* `t.attributes` lists the attributes of the type, as slots (see below).
* `t.features` lists the attributes and references of the type.
* `t.metamodel` retrieves the metamodel that defines the type.
* `t.name` retrieves the name of the type.
* `t.node` returns the underlying `IGraphNode`.
* `t.references` lists the references of the type, as slots.

## Slots

For a slot `sl`, these attributes are available:

* `sl.instanceTypeName` (since 2.3.0): name of the Java instance type for values of this slot.
* `sl.isAttribute`: true if and only if this is an attribute slot.
* `sl.isMany`: true if and only if this is a multi-valued slot.
* `sl.isOrdered`: true if and only if the values should follow some order.
* `sl.isReference`: true if and only if this is a reference slot.
* `sl.isUnique`: true if and only if the value for this slot should be unique within its model.
* `sl.name`: name of the slot.
* `sl.type` (since 2.3.0): for reference slots, returns the [type](#types) of the target(s). `null` for attribute slots.

## Files

For a file `f`, these attributes are available:

* `f.contents`: returns all the model elements in the file.
* `f.node`: returns the underlying `IGraphNode`.
* `f.path`: returns the path of the file within the repository (e.g. `/input.xmi`).
* `f.repository`: returns the URL of the repository (e.g. `file:///home/myuser/models`).
* `f.roots`: returns the root model elements in the file.
