[Oomph](https://projects.eclipse.org/projects/tools.oomph) has a feature that synchronizes preferences across workspaces (see [bug 490549](https://bugs.eclipse.org/bugs/show_bug.cgi?id=490549)).

This can be a problem if you expect different workspaces to have different Hawk indexes. If so, you should reconfigure Oomph so it does not record the `/instance/org.eclipse.hawk.osgiserver` preferences node at the "User" and "Installation" levels.

To do this, go to "Window > Preferences", select "Oomph > Setup Tasks > Preference Recorder", check "Record into", select "User" and make sure `/instance/org.eclipse.hawk.osgiserver/config` either does not appear or is *unchecked*. It should be the same for "Installation" and "Workspace".

![Oomph configuration](img/oomph.png)
