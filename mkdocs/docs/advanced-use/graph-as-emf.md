In addition to regular querying, it is possible to use a Hawk graph as a model itself. To do so, use the "File > New > Other > Hawk > Local Hawk Model Descriptor" wizard and select the Hawk instance you want to access as a model.

Once the wizard is finished, open the `.localhawkmodel` file to browse through it as an EMF model. You will need to ensure that the EPackages of the indexed models are part of your EMF package registry: normally Hawk should ensure this happens. For a Hawk index containing the GraBaTs 2009 `set0.xmi` file, it will look like this:

![Hawk EMF resource showing set0.xmi](img/hawk_emfresource.png)

The actual editor is a customized version of the [Epsilon Exeed](http://www.eclipse.org/epsilon/doc/articles/inspect-models-exeed/) editor, which is based on the standard EMF reflective tree-based editor. The contents of the graph are navigated lazily, so we can open huge models very quickly and navigate through them.

The editor also provides additional "Custom" actions when we right click on a top-level node (usually labelled with URLs). Currently, it supports an efficient `Fetch by EClass` method, that allows fetching all the instances of a type immediately, without having to load the rest of the model. Future versions of Hawk may expose additional operations through this menu.

Finally, the EMF resource can be used normally from any EMF-based tools (e.g. transformation engines). However, to make the most out of the resources it will be necessary to extend the tools to have them integrate the efficient graph-based operations that are not part of the EMF Resource interface.
