Hawk can be used as a regular Java library (to be embedded within another Java program) or as a set of plugins for the [Eclipse](http://www.eclipse.org/) IDE.

To install most of Hawk's Eclipse plugins, point your installation to this update site:

    https://download.eclipse.org/hawk/{{ stable.version }}/updates/

This is a composite update site, which contains not only Hawk, but also its dependencies. Check all the categories that start with "Hawk".

Some of the components in Hawk cannot be redistributed in binary form due to incompatible licenses. You will need to build the update site for these restricted components yourself: please consult the developer resources in the wiki to do that.
