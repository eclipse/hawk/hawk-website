# Example queries on XMI models

These are some sample queries that can be done on any set of indexed XMI-based UML models, assuming that `Class::name` has been added as an indexed attribute and `Class::ownedOperationCount` has been defined as a derived attribute (as showed in [the core concepts page](core-concepts.md)). All the queries are written in the [Epsilon Object Language](http://www.eclipse.org/epsilon/doc/eol/).

In order to index XMI-based UML models, you only need to enable the `UMLMetaModelResourceFactory` and `UMLModelResourceFactory` plugins when you create a new Hawk instance, and ensure your files have the `.uml` extension. If you are using any predefined UML data types, you may also want to add a `PredefinedUMLLibraries` location inside "Indexed Locations": that will integrate those predefined objects into the Hawk graph, allowing you to reference them on queries.

The rest of this article will run on [this toy XMI-based UML file](https://raw.githubusercontent.com/mondo-project/mondo-hawk/gh-pages/examples/models/zoo/zoo.xmi), which was exported from [this Modelio 3.2.1 project](../assets/Zoo.modelio.zip):

![Example UML model](img/zoo-classes.png)

To avoid ambiguity in type names, the default namespaces list in the query dialog should include the UML metamodel URI (`http://www.eclipse.org/uml2/5.0.0/UML` for the above `UML.ecore` file).

## All instances of a type

    return Class.all.size;

Returns the total number of classes within the specified scope. If you leave "Context Files" empty, it'll count all the classes in all the projects. If you put "*OSS.modelio.zip" in "Context Files", it'll count only the classes within the OSS project. This is faster than going through the model because we can go to the Class node and then simply count all the incoming edges with label "ofType".

You should obtain this result:

```
6
```

## Reference slots in a type

    return Model.types.selectOne(t|t.name='Class').references;

Gives you all the reference slots in the UML "Class" type. This is an example of the queries that can be performed at the "meta" level: more details are available in [[Meta level queries in Hawk]]. The result would look like this:

```
[Slot [typeNode=...], Slot [...], ...]
```

## Reference traversal

    return Class.all
      .select(c|c.qualifiedName='zoo::Zebra')
      .superClass.flatten.name;

Gives you the names of all the superclasses of class `Zebra` within model `zoo`:

```
[Animal]
```

## Reverse reference traversal

    return Class.all
      .select(c|c.qualifiedName='zoo::Animal')
      .revRefNav_superClass.flatten.name;

Gives the names of all the *sub*classes of `Animal` (follows "superClass" in reverse). The UML metamodel doesn't have "subclass" links, but we can use Hawk's automatic support for reverse traversal of references. In general, if `x.e` is a reference, we can follow it in reverse with `x.revRefNav_e`. We can also access containers using `x.eContainer`.

Result:

```
[Elephant, Zebra, Lion]
```

## Range queries with indexed or derived integer attributes

    return Class.all.select(c|c.ownedOperationCount > 0).name;

Finds the names of the classes with at least one operation of their own.

Result:

```
[Lion, Animal, Elephant, Zebra]
```

## Advanced example: loops, variables and custom operations

    var counts = Sequence {};
    var i = 0;
    var n = count(0);
    while (n > 0) {
      counts.add(Sequence {"> " + i, n});
      i = i + 1;
      n = count(i);
    }

    return counts;

    operation count(n) {
      return Class.all.select(c|c.ownedOperationCount > n).size;
    }

This query produces a sequence of `>x, y` pairs which indicate that `y` classes have more than `x` operations of their own.

Result:

```
[[> 0, 4], [> 1, 1], [> 2, 1]]
```
