# Example queries on Modelio models

This article shows several example queries on Modelio projects. The Modelio model driver does not use the XMI export in Modelio: instead, it parses `.exml` files directly (which might be contained in `.ramc` files) and understands metamodels described in Modelio `metamodel_descriptor.xml` files. (To obtain one, download the source code for your Modelio version and search within it. [Here](https://gitlab.eclipse.org/eclipse/hawk/hawk/-/raw/master/core/tests/org.eclipse.hawk.modelio.exml.tests/resources/metamodel/metamodel_descriptor.xml) is a copy of the one used for Modelio 3.6.)

All the queries are written in the [Epsilon Object Language](http://www.eclipse.org/epsilon/doc/eol/), and assume that the [toy Zoo Modelio project](../assets/Zoo.modelio.zip) has been indexed. The queries are based on those in [the XMI-based UML examples page](papyrus.md). The underlying UML model looks like this:

![Example UML model](img/zoo-classes.png)

To avoid ambiguity in type names, the default namespaces list in the query dialog should include `modelio://uml::statik`.

# All instances of a type

Returns the number of instances of "Class" in the index:

``` javascript
return Class.all.size;
```

# Metamodel URI for the "Class" type

Returns the URI of the metamodel that contains the "Class" type (`modelio://uml::statik`):

``` javascript
return Model.types.selectOne(t|t.name = 'Class').metamodel.uri;
```

# Reference slots in a type

Returns the reference slots in the type "Class":

``` javascript
return Model.types.select(t|t.name='Class').references;
```

# Reference traversal

Returns the superclass of "Zebra" by navigating the "Parent" and "SuperType" associations present in the Modelio metamodel:

``` javascript
return Class.all
  .selectOne(c|c.Name='Zebra')
  .Parent.SuperType.Name;
```

# Reverse reference traversal

Returns the subclasses of "Animal", using the `revRefNav_` to navigate references in reverse:

``` javascript
return Class.all
  .selectOne(c|c.Name='Animal')
  .revRefNav_SuperType
  .revRefNav_Parent
  .Name;
```

# Range queries with indexed or derived integer attributes

This example requires adding a derived attribute first:

* Metamodel URI: `modelio://uml::statik`
* Type Name: `Class`
* Attribute Name: `ownedOperationCount`
* Attribute Type: `Integer`
* isMany, isOrdered, isUnique: `false`
* Derivation Language: `EOLQueryEngine`
* Derivation Logic: `return self.OwnedOperation.size;`

After it has been added, this query will return the classes that have one or more operations:

``` javascript
return Class.all.select(c|c.ownedOperationCount > 0).Name;
```

# Advanced example: loops, variables and custom operations

This query produces a sequence of `>x, y` pairs which indicate that `y` classes have more than `x` operations of their own:

``` javascript
var counts = Sequence {};
var i = 0;
var n = count(0);
while (n > 0) {
  counts.add(Sequence {">" + i, n});
  i = i + 1;
  n = count(i);
}

return counts;

operation count(n) {
  return Class.all.select(c|c.ownedOperationCount > n).size;
}
```
