## Screencasts

We have several screencasts that show how to use Hawk and work on its code:

* [Running of basic operations](https://www.youtube.com/watch?v=hQbkA0jmBTY)
* [Use of advanced features](https://www.youtube.com/watch?v=pGL2-lJ0HAg)
* [Download and configuration of Hawk onto a fresh Eclipse Luna (Modeling Tools) distribution](https://www.youtube.com/watch?v=d_DqR-0v_4s)
* [How to use Hawk to add Modelio metamodel(s)](https://www.youtube.com/watch?v=63kg62HxgD4)
* [Use Hawk Server to auto-configure & start Hawk Instances](https://www.youtube.com/watch?v=swAgG8v-lLU)

## Papers

Hawk has been discussed and evaluated across a long series of papers.
These are listed in chronological order (from oldest to newest):

* [Hawk: towards a scalable model indexing architecture](http://dl.acm.org/citation.cfm?id=2487771)
* [A Framework to Benchmark NoSQL Data Stores for Large-Scale Model Persistence](http://link.springer.com/chapter/10.1007%2F978-3-319-11653-2_36)
* [Towards Scalable Querying of Large-Scale Models](http://link.springer.com/chapter/10.1007%2F978-3-319-09195-2_3)
* [Evaluation of Contemporary Graph Databases for Efficient Persistence of Large-Scale Models](http://www.jot.fm/contents/issue_2014_07/article3.html)
* [Towards Incremental Updates in Large-Scale Model Indexes](http://link.springer.com/chapter/10.1007%2F978-3-319-21151-0_10)
* [Towards Scalable Model Indexing (PhD Thesis)](http://etheses.whiterose.ac.uk/14376/)
* [Stress-testing remote model querying APIs for relational and graph-based stores](http://dx.doi.org/10.1007/s10270-017-0606-9)
* [Integration of a graph-based model indexer in commercial modelling tools](https://dl.acm.org/citation.cfm?doid=2976767.2976809)
* [Integration of Hawk for Model Metrics into the MEASURE Platform](https://doi.org/10.5220/0006732207190730)
* [Hawk solutions to the TTC 2018 Social Media Case](http://ceur-ws.org/Vol-2310/paper7.pdf)
* [Scaling-up domain-specific modelling languages through modularity services](https://doi.org/10.1016/j.infsof.2019.05.010)
* [Querying and Annotating Model Histories with Time-Aware Patterns](https://doi.org/10.1109/MODELS.2019.000-2)
* [Scalable modeling technologies in the wild: an experience report on wind turbines control applications development](http://doi.org/10.1007/s10270-020-00776-8)
* Book chapter: [Monitoring model analytics over large repositories with Hawk and MEASURE](https://doi.org/10.1016/B978-0-12-816649-9.00014-4)
* [Temporal Models for History-Aware Explainability](https://doi.org/10.1145/3419804.3420276)
* [From a Series of (Un)fortunate Events to Global Explainability of Runtime Model-Based Self-Adaptive Systems](https://doi.org/10.1109/MODELS-C53483.2021.00127)
* [Incremental execution of temporal graph queries over runtime models with history and its applications](https://doi.org/10.1007/s10270-021-00950-6)
* [Reward-Reinforced Generative Adversarial Networks for Multi-Agent Systems](https://doi.org/10.1109/TETCI.2021.3082204)
* [Event-driven temporal models for explanations - ETeMoX: explaining reinforcement learning](https://doi.org/10.1007/s10270-021-00952-4)
* [A cross-technology benchmark for incremental graph queries](https://doi.org/10.1007/s10270-021-00927-5)

## Slides

* [Hawk: indexado de modelos en bases de datos NoSQL](https://bitbucket.org/bluezio/hawk-cadiz2018/overview) - 90 minute slides in Spanish about the MONDO project and Hawk
* [MODELS18 tutorial on NeoEMF and Hawk](https://github.com/SOM-Research/hawk-neoemf-models-2018-tutorial)

## Related tools

* The [HawkQuery SMM MEASURE library](https://github.com/Orjuwan-alwadeai/HawkQuerySMMMMeasureLib) allows using Hawk servers as metric providers for the MEASURE platform.
