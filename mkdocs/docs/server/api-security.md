In some cases, we may want to protect the API from unaccounted use, as clients would have access to potentially sensitive information. In order to provide this access control, the [Apache Shiro](https://shiro.apache.org/) library has been integrated transparently as a filter for all incoming requests to the endpoints under `/thrift`. `/thrift-local` endpoints are not password-protected, as they only answer requests from other processes in the machine hosting the MONDO Server.

Apache Shiro protects these `/thrift` endpoints using standard HTTP Basic authentication, which is transparent to Thrift, avoiding the need to pollute the web API with access tokens in every single method. Industrial partners will be instructed to always use the authentication layer in combination with SSL, since HTTP Basic by itself is insecure.

One important advantage of Shiro is its configurability through a single `.ini` file, like this one:

```
[main]
# Objects and their properties are defined here,
# Such as the securityManager, Realms and anything
# else needed to build the SecurityManager

# Note: this should be set to true in production!
ssl.enabled = true

# Toggle to enable/disable authentication completely
authcBasic.enabled = true

# Use Hawk realm
mondoRealm = uk.ac.york.mondo.integration.server.users.servlet.shiro.UsersRealm
securityManager.realms = $mondoRealm

# We’re using SHA−512 for passwords, with 10k iterations
credentialsMatcher = org.apache.shiro.authc.credential.Sha512CredentialsMatcher
credentialsMatcher.hashIterations = 10000
mondoRealm.credentialsMatcher = $credentialsMatcher

[urls]
/thrift/** = ssl, authcBasic
```

Shiro is heavily componentized, making it easy to provide alternative implementations of certain pieces and reuse the default implementations for the rest. In the shown example, all requests to the `/thrift` endpoints go through the default `ssl` and `authcBasic` filters: when enabled, these filters enforce the use of SSL and HTTP Basic authentication respectively. Both filters should be enabled in production environments.

For the HTTP Basic authentication, the server provides its own implementation of a Shiro security realm, which is dedicated to storing and retrieving user details. The security realm uses an embedded [MapDB](http://www.mapdb.org/) database to persist these user details, which are managed through the Users service (Section 5.2.4). An embedded database was used in order to prevent end users from having to set up a database just to store a small set of users. MapDB is distributed as a single `.jar` file, making it very simple to integrate. In any case, the realm could be replaced with another one if desired by editing `shiro.ini` on an installation.

Passwords for the MONDO realm are stored in a hashed and salted form, using 10000 iterations of SHA-512 and a random per-password salt.

As for the client side, the command-line based clients accept optional arguments for the required credentials when connecting to the Thrift endpoints. If the password is omitted, the command-line based clients will require it in a separate "silent" prompt that does not show the characters that are typed, preventing shoulder surfing attacks. Due to limitations in the Eclipse graphical user interface, these silent prompts are only available when running the command-line based clients from a proper terminal window and not from the Eclipse "Console" view.

The graphical clients connect to the Thrift endpoints using “lazy” credential providers: if authentication is required, they will attempt to retrieve previously used credentials from the Eclipse secure store and if no such credentials exist, they will show an authentication dialog asking for the username and password to be used. The Eclipse secure storage takes advantage of the access control and encryption capabilities of the underlying operating system as much as possible, and makes it possible to store passwords safely and conveniently. These stored MONDO server credentials can be managed from the "Hawk Servers" preference page.

Regarding the Artemis messaging queue, it has been secured with the same Shiro realm as the Thrift endpoints. The remote Hawk EMF abstraction (the only component that uses Artemis within the MONDO platform) will connect to Artemis with the same credentials that were used to connect to Thrift, if authentication was required.
