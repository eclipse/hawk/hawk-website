The API can be used from any programming language that Apache Thrift can generate client stubs for.
In the [download page](https://eclipse.org/hawk), you will find a few client libraries already generated for you: at the time of writing, these include C++, Python, and JavaScript.

The Hawk Git repository includes several example programs using these libraries:

* [C++](https://gitlab.eclipse.org/eclipse/hawk/hawk/-/tree/master/docs/org.eclipse.hawk.examples.cpp)
* [JavaScript](https://gitlab.eclipse.org/eclipse/hawk/hawk/-/tree/master/docs/org.eclipse.hawk.examples.js)
* [Python](https://gitlab.eclipse.org/eclipse/hawk/hawk/-/tree/master/docs/org.eclipse.hawk.examples.python)

If you need an example in another programming language, please let us know at the [forum](https://www.eclipse.org/forums/index.php/f/442/).
