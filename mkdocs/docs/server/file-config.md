Hawk server includes an API to add Hawk instances that are used to index and query models. The configuration engine allows the server to create and configure Hawk instances as per user-created configuration files. The server should be ready to receive user queries upon startup without any interaction from user or clients. 
Upon startup, Hawk server reads and parses configuration files, and then it creates/updates hawk instances as per configuration files. 

NOTE: the Hawk server no longer writes to configuration files. If an instance configuration changes during operation, this configuration is persisted through the current HawkConfig mechanism. Configuration files will not overwrite any of the changed settings. The only exception is the polling min/max which will revert to config file settings if a server is restarted.

## Format

Configuration files are XML files that define hawk instance name and its configuration. An XML schema can be found at
[HawkServerConfigurationSchema.xsd](https://github.com/Orjuwan-alwadeai/mondo-hawk/releases/download/v1.1.0-201708-interim/HawkServerConfigurationSchema.xsd). A sample configuration file can be found at [Sample Configuration File](https://github.com/Orjuwan-alwadeai/mondo-hawk/releases/download/v1.1.0-201708-interim/Sample_config_file.xml)

The XML should include the following elements:

### Table 1: List of XML elements in configuration file

| Element Name | Parent Element Name| multiplicity | Value | Description | 
| ------------- | ----------------- | ------------- | -------| -------------| 
| ‘hawk’ | xml | 1 | None | Root element| 
| ‘delay’ | ‘hawk’ | 1 | None | Polling configuration| 
| ‘plugins’| ‘hawk’ | 0-1 | None | List of plugins (to be/that are) enabled| 
| ‘plugin’| ‘plugins’ | 0-* | None | Plugin name| 
| ‘metamodels’ | ‘hawk’ | 0-1 | None | List of metamodels (to be/that are) registered| 
| ‘metamodel’ | ‘metamodels’ | 0-* | None | Metamodel parameters| 
| ‘repositories’ | ‘hawk’ | 0-1 | None | List of repositories (to be/that are) added| 
| ‘repository’ | ‘repositories’|  0-* | None | Repository parameters| |
| ‘derivedAttributes’ | ‘hawk’ | 0-1 | None | List of derived attributes (to be/that are) added| 
| ‘derivedAttribute’ | ‘derivedAttributes’ | 0-* | None |Derived attribute parameters| 
| ‘derivation’ | ‘derivedAttribute’ | 0-1 | None | Derivation parameters| 
| ‘logic’ | ‘derivation’ | 0-1 | CDATA section | An executable expression of the derivation logic in the language specified.| 
| ‘indexedAttributes’ | ‘hawk’ | 0-1 | None | List of indexed attributes (to be/that are) added| 
| ‘indexedAttribute’ | ‘indexedAttributes’ | 0-* | None | Indexed attribute parameters| 

### Table 2:	 ‘hawk’ attributes

|Element Name| Attribute name|Optional/Required |Type|Description|
|------------|---------------|------------------|----|-----------|
|**‘hawk’**      |  ‘name’	| Required |String |The unique name of the new Hawk instance |
|       |‘backend’ |Required |String |The name of the backend to be used (e.g.org.eclipse.hawk.orientdb.OrientDatabase, org.eclipse.hawk.orientdb.RemoteOrientDatabase)|

### Table 3:	 ‘delay’ attributes

|Element Name| Attribute name|Optional/Required |Type|Description|
|------------|---------------|------------------|----|-----------|
|**‘delay’**	|‘min’	|Required	|String	|Minimum delay between periodic synchronization in milliseconds|
|	|‘max’	|Required	|String	|Maximum delay between periodic synchronization in milliseconds (0 to disable periodic synchronization)|

### Table 4:	 ‘plugin’ attributes

|Element Name| Attribute name|Optional/Required |Type|Description|
|------------|---------------|------------------|----|-----------|
|**‘plugin’**	|‘name’	|Required	|String|	e.g. (org.eclipse.hawk.modelio.exml.listeners.ModelioGraphChangeListener, org.eclipse.hawk.modelio.exml.metamodel.ModelioMetaModelResourceFactory, org.eclipse.hawk.modelio.exml.model.ModelioModelResourceFactory)|

### Table 5:	 ‘metamodel’ attributes

|Element Name| Attribute name|Optional/Required |Type|Description|
|------------|---------------|------------------|----|-----------|
|**‘metamodel’**|	‘location’|	Optional|	String|	Location of metamodel file to be registered |
|	|~~‘uri’~~	|~~Optional~~|	~~String~~	|~~Metamodel URI. This value is set automatically by server to list registered metamodels~~  |

### Table 6:	 ‘repository’ attributes

|Element Name| Attribute name|Optional/Required |Type|Description|
|------------|---------------|------------------|----|-----------|
|**‘repository’**|‘location’	|Required|	String|	Location of the repository|
|            |‘type’|Optional|String| The type of repository available repository types ()|
|            |‘user’|Optional|String| Username for logging into the VCS|
|            |‘pass’|Optional|String| Password for logging into the VCS|
|            |‘frozen’|Optional|String| If the repository is frozen (true/false)|

### Table 7:	‘derivedAttribute’ attributes

|Element Name| Attribute name|Optional/Required |Type|Description|
|------------|---------------|------------------|----|-----------|
|**‘derivedAttribute’**|	‘attributeName’	|Required	|String|	The name of the derived attribute|
||	‘typeName’ |	Required	|String|	The name of the type to which the derived attribute belongs|		
||‘metamodelUri’|	Required	|String|	The URI of the metamodel to which the de- rived attribute belongs|
||	‘attributeType’|	Optional	|String|	The (primitive) type of the derived attribute|
||	‘isOrdered’	|Optional	|String|	A flag specifying whether the order of the values of the derived attribute is significant (only makes sense when isMany=true)|
||	‘isUnique’	|Optional	|String|	A flag specifying whether the the values of the derived attribute are unique (only makes sense when isMany=true)|
||	‘isMany’	|Optional	|String|	The multiplicity of the derived attribute|

### Table 8:	 ‘derivation’ attributes

|Element Name| Attribute name|Optional/Required |Type|Description|
|------------|---------------|------------------|----|-----------|
|**‘derivation’**	|‘language’|	Required|	String	|The language used to express the derivation logic. Available labguages in Hawk: org.eclipse.hawk.epsilon.emc.EOLQueryEngine, org.eclipse.hawk.orientdb.query.OrientSQLQueryEngine, org.eclipse.hawk.epsilon.emc.EPLQueryEngine |

### Table 9:	 ‘indexedAttribute’ attributes

|Element Name| Attribute name|Optional/Required |Type|Description|
|------------|---------------|------------------|----|-----------|
|**‘indexedAttribute’**|	‘attributeName’|	Required|	String	|The name of the indexed attribute.|
||	‘typeName’ 	|Required	|String	|The name of the type to which the indexed attribute|
||	‘metamodelUri’	|Required	|String	|The URI of the metamodel to which the indexed attribute belongs.|

## Location

Configuration files are expected to be located in the ‘configuration’ folder in the server’s home directory. Each Hawk instance should have its own configuration file. There are no rules on how the file should be named. It is a good practice to include hawk instance name in the file name for easy recognition. 
![](img/config-file-location.png)

## How to use/enable Hawk instance configuration engine

You can follow [this video tutorial](https://youtu.be/swAgG8v-lLU), or alternatively follow these steps:

1.	Download the hawk-server-*.zip file for your operating system and architecture of choice from [Hawk Server With Configuration](https://github.com/Orjuwan-alwadeai/mondo-hawk/releases/tag/v1.1.0-201708-interim))
2.	Create a configuration file for each instance required to run in the Hawk server. 
3.	Edit configuration files: 
* Set instance name, backend, delay
* Add list of plugins to be enabled
* Add metamodel file to location to be registered
* Add repositories that are to be indexed 
* Add any required derived attributes
* Add any required indexed attributes
4.	Save the configuration files to the ‘configuration’ folder in the server’s home directory (see figure 1)
5.	Perform any other configuration that are required by Hawk Server and start the server, by following the [deployment instructions](deployment.md)].
6.	Check if the hawk instances are added and running by typing ‘hawkListInstances’ in the server’s command terminal:
 ![](img/hawklistinstances.png)

## Usage Notes

* Deleting configuration files from the directory will not delete instances from the server. However, the server will not start those instances.

* To test Hawk server with Measure Platform, refer to [Using HawkQueryMeasure to query Hawk instance running in Hawk Server](https://github.com/Orjuwan-alwadeai/HawkQuerySMMMMeasureLib/wiki/Using-HawkQueryMeasure--to-query-Hawk-instance-running-in-Hawk-Server)
