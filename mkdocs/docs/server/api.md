## Services

### Users

The majority of service operations provided by the server
        require user authentication (indicated in the top-left
        cell of each operation table) to prevent unaccountable use.
        As such, the platform needs to provide basic user management service operations
        for creating, updating and deleting user accounts. When handling passwords, only SSL should be used, as otherwise they could be intercepted.


#### Users.createUser ####

Creates a new platform user.

Returns `void`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| username  | string | A unique identifier for the user. |
| password  | string | The desired password. |
| profile  | UserProfile | The profile of the user. |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| UserExists | The specified username already exists. |

#### Users.updateProfile ####

Updates the profile of a platform user.

Returns `void`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| username  | string | The name of the user to update the profile of. |
| profile  | UserProfile | The updated profile of the user. |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| UserNotFound | The specified username does not exist. |

#### Users.updatePassword ####

Updates the password of a platform user.

Returns `void`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| username  | string | The name of the user to update the profile of. |
| newPassword  | string | New password to be set. |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| UserNotFound | The specified username does not exist. |

#### Users.deleteUser ####

Deletes a platform user.

Returns `void`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| username  | string | The name of the user to delete. |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| UserNotFound | The specified username does not exist. |

### Hawk

The following service operations expose the capabilities of the Hawk heterogeneous model indexing framework.


#### Hawk.createInstance ####

Creates a new Hawk instance (stopped).

Returns `void`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| name  | string | The unique name of the new Hawk instance. |
| backend  | string | The name of the backend to be used, as returned by listBackends(). |
| minimumDelayMillis  | i32 | Minimum delay between periodic synchronization in milliseconds. |
| maximumDelayMillis  | i32 | Maximum delay between periodic synchronization in milliseconds (0 to disable periodic synchronization). |
| enabledPlugins  (optional) | list<string> | List of plugins to be enabled: if not set, all plugins are enabled. |
| indexFactory  (optional) | string | Factory to be used: if not set, the standard LocalHawkFactory is used. |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| HawkFactoryNotFound | No Hawk factory exists with that name. |

#### Hawk.listBackends ####

Lists the names of the available storage backends.

Returns `list<string>`.  Does not take any parameters.
#### Hawk.listPlugins ####

Lists all the Hawk plugins that can be enabled or disabled: metamodel parsers, model parsers and graph change listeners.

Returns `list<string>`.  Does not take any parameters.
#### Hawk.listPluginDetails ####

Lists all the Hawk plugins that can be enabled or disabled, with details about each of them.

Returns `list<HawkPlugin>`.  Does not take any parameters.
#### Hawk.listInstances ####

Lists the details of all Hawk instances.

Returns `list<HawkInstance>`.  Does not take any parameters.
#### Hawk.removeInstance ####

Removes an existing Hawk instance.

Returns `void`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| name  | string | The name of the Hawk instance to remove. |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| HawkInstanceNotFound | No Hawk instance exists with that name. |

#### Hawk.startInstance ####

Starts a stopped Hawk instance.

Returns `void`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| name  | string | The name of the Hawk instance to start. |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| HawkInstanceNotFound | No Hawk instance exists with that name. |

#### Hawk.stopInstance ####

Stops a running Hawk instance.

Returns `void`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| name  | string | The name of the Hawk instance to stop. |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| HawkInstanceNotFound | No Hawk instance exists with that name. |
| HawkInstanceNotRunning | The selected Hawk instance is not running. |

#### Hawk.syncInstance ####

Forces an immediate synchronization on a Hawk instance.

Returns `void`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| name  | string | The name of the Hawk instance to stop. |
| blockUntilDone  (optional) | bool | If true, blocks the call until the synchronisation completes. False by default. |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| HawkInstanceNotFound | No Hawk instance exists with that name. |
| HawkInstanceNotRunning | The selected Hawk instance is not running. |

#### Hawk.registerMetamodels ####

Registers a set of file-based metamodels with a Hawk instance.

Returns `void`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| name  | string | The name of the Hawk instance. |
| metamodel  | list<File> | The metamodels to register. More than one metamodel file can be provided in one request, to accomodate fragmented metamodels. |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| HawkInstanceNotFound | No Hawk instance exists with that name. |
| InvalidMetamodel | The provided metamodel is not valid (e.g. unparsable or inconsistent). |
| HawkInstanceNotRunning | The selected Hawk instance is not running. |

#### Hawk.unregisterMetamodels ####

Unregisters a metamodel from a Hawk instance.

Returns `void`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| name  | string | The name of the Hawk instance. |
| metamodel  | list<string> | The URIs of the metamodels. |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| HawkInstanceNotFound | No Hawk instance exists with that name. |
| HawkInstanceNotRunning | The selected Hawk instance is not running. |

#### Hawk.listMetamodels ####

Lists the URIs of the registered metamodels of a Hawk instance.

Returns `list<string>`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| name  | string | The name of the Hawk instance. |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| HawkInstanceNotFound | No Hawk instance exists with that name. |
| HawkInstanceNotRunning | The selected Hawk instance is not running. |

#### Hawk.listTypeNames ####

Lists the names of the types registered for a certain metamodel in an instance.

Returns `list<string>`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| hawkInstanceName  | string | The name of the Hawk instance. |
| metamodelURI  | string | The URI of the metamodel. |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| HawkInstanceNotFound | No Hawk instance exists with that name. |
| HawkInstanceNotRunning | The selected Hawk instance is not running. |
| HawkMetamodelNotFound | The selected Hawk instance does not have a metamodel with that URI. |

#### Hawk.listAttributeNames ####

Lists the names of the attributes in a type registered for a certain metamodel in an instance.

Returns `list<string>`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| hawkInstanceName  | string | The name of the Hawk instance. |
| metamodelURI  | string | The URI of the metamodel. |
| typeName  | string | The name of the type. |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| HawkInstanceNotFound | No Hawk instance exists with that name. |
| HawkInstanceNotRunning | The selected Hawk instance is not running. |
| HawkMetamodelNotFound | The selected Hawk instance does not have a metamodel with that URI. |
| HawkTypeNotFound | The selected Hawk instance does not have a type with that name. |

#### Hawk.listMetamodelParsers ####

Lists the details of the metamodel parsers in a Hawk instance.

Returns `list<MetamodelParserDetails>`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| name  | string | The name of the Hawk instance. |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| HawkInstanceNotFound | No Hawk instance exists with that name. |
| HawkInstanceNotRunning | The selected Hawk instance is not running. |

#### Hawk.listQueryLanguages ####

Lists the supported query languages and their status.

Returns `list<string>`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| name  | string | The name of the Hawk instance. |

#### Hawk.query ####

Runs a query on a Hawk instance and returns a sequence of scalar values and/or model elements.

Returns `QueryResult`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| name  | string | The name of the Hawk instance. |
| query  | string | The query to be executed. |
| language  | string | The name of the query language used (e.g. EOL, OCL). |
| options  | HawkQueryOptions | Options for the query. |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| HawkInstanceNotFound | No Hawk instance exists with that name. |
| HawkInstanceNotRunning | The selected Hawk instance is not running. |
| UnknownQueryLanguage | The specified query language is not supported by the operation. |
| InvalidQuery | The specified query is not valid. |
| FailedQuery | The specified query failed to complete its execution. |

#### Hawk.timedQuery ####

Runs a query on a Hawk instance and returns a sequence of scalar values and/or model elements.

Returns `QueryReport`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| name  | string | The name of the Hawk instance. |
| query  | string | The query to be executed. |
| language  | string | The name of the query language used (e.g. EOL, OCL). |
| options  | HawkQueryOptions | Options for the query. |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| HawkInstanceNotFound | No Hawk instance exists with that name. |
| HawkInstanceNotRunning | The selected Hawk instance is not running. |
| UnknownQueryLanguage | The specified query language is not supported by the operation. |
| InvalidQuery | The specified query is not valid. |
| FailedQuery | The specified query failed to complete its execution. |

#### Hawk.resolveProxies ####

Returns populated model elements for the provided proxies.

Returns `list<ModelElement>`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| name  | string | The name of the Hawk instance. |
| ids  | list<string> | Proxy model element IDs to be resolved. |
| options  | HawkQueryOptions | Options for the query. |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| HawkInstanceNotFound | No Hawk instance exists with that name. |
| HawkInstanceNotRunning | The selected Hawk instance is not running. |

#### Hawk.asyncQuery ####

Schedules a query to run on a Hawk instance, returning a UUID for later reference.

Returns `string`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| name  | string | The name of the Hawk instance. |
| query  | string | The query to be executed. |
| language  | string | The name of the query language used (e.g. EOL, OCL). |
| options  | HawkQueryOptions | Options for the query. |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| HawkInstanceNotFound | No Hawk instance exists with that name. |
| HawkInstanceNotRunning | The selected Hawk instance is not running. |
| UnknownQueryLanguage | The specified query language is not supported by the operation. |
| InvalidQuery | The specified query is not valid. |

#### Hawk.cancelAsyncQuery ####

Cancels a running query in the Hawk server, freeing its resources.

Returns `void`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| queryID  | string | The query ID returned by asyncQuery. |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| InvalidQuery | The specified query is not valid. |

#### Hawk.fetchAsyncQueryResults ####

Fetches the results from a scheduled query, freeing its resources. Blocks until the query has completed.

Returns `QueryReport`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| queryID  | string | The query ID returned by asyncQuery. |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| InvalidQuery | The specified query is not valid. |
| FailedQuery | The specified query failed to complete its execution. |

#### Hawk.addRepository ####

Asks a Hawk instance to start monitoring a repository.

Returns `void`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| name  | string | The name of the Hawk instance. |
| repo  | Repository | The repository to monitor. |
| credentials  (optional) | Credentials | A valid set of credentials that has read-access to the repository. |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| HawkInstanceNotFound | No Hawk instance exists with that name. |
| HawkInstanceNotRunning | The selected Hawk instance is not running. |
| UnknownRepositoryType | The specified repository type is not supported by the operation. |
| VCSAuthenticationFailed | The client failed to prove its identity in the VCS. |

#### Hawk.isFrozen ####

Returns true if a repository is frozen, false otherwise.

Returns `bool`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| name  | string | The name of the Hawk instance. |
| uri  | string | The URI of the repository to query. |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| HawkInstanceNotFound | No Hawk instance exists with that name. |
| HawkInstanceNotRunning | The selected Hawk instance is not running. |

#### Hawk.setFrozen ####

Changes the 'frozen' state of a repository.

Returns `void`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| name  | string | The name of the Hawk instance. |
| uri  | string | The URI of the repository to be changed. |
| isFrozen  | bool | Whether the repository should be frozen (true) or not (false). |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| HawkInstanceNotFound | No Hawk instance exists with that name. |
| HawkInstanceNotRunning | The selected Hawk instance is not running. |

#### Hawk.removeRepository ####

Asks a Hawk instance to stop monitoring a repository and remove its elements from the graph.

Returns `void`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| name  | string | The name of the Hawk instance. |
| uri  | string | The URI of the repository to stop monitoring. |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| HawkInstanceNotFound | No Hawk instance exists with that name. |
| HawkInstanceNotRunning | The selected Hawk instance is not running. |

#### Hawk.updateRepositoryCredentials ####

Changes the credentials used to monitor a repository.

Returns `void`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| name  | string | The name of the Hawk instance. |
| uri  | string | The URI of the repository to update. |
| cred  | Credentials | The new credentials to be used. |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| HawkInstanceNotFound | No Hawk instance exists with that name. |
| HawkInstanceNotRunning | The selected Hawk instance is not running. |

#### Hawk.listRepositories ####

Lists the repositories monitored by a Hawk instance.

Returns `list<Repository>`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| name  | string | The name of the Hawk instance. |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| HawkInstanceNotFound | No Hawk instance exists with that name. |
| HawkInstanceNotRunning | The selected Hawk instance is not running. |

#### Hawk.listRepositoryTypes ####

Lists the available repository types in this installation.

Returns `list<string>`.  Does not take any parameters.
#### Hawk.listFiles ####

Lists the paths of the files of the indexed repository.

Returns `list<string>`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| name  | string | The name of the Hawk instance. |
| repository  | list<string> | The URI of the indexed repository. |
| filePatterns  | list<string> | File name patterns to search for (* lists all files). |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| HawkInstanceNotFound | No Hawk instance exists with that name. |
| HawkInstanceNotRunning | The selected Hawk instance is not running. |

#### Hawk.configurePolling ####

Sets the base polling period and max interval of a Hawk instance.

Returns `void`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| name  | string | The name of the Hawk instance. |
| base  | i32 | The base polling period (in seconds). |
| max  | i32 | The maximum polling interval (in seconds). |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| HawkInstanceNotFound | No Hawk instance exists with that name. |
| HawkInstanceNotRunning | The selected Hawk instance is not running. |
| InvalidPollingConfiguration | The polling configuration is not valid. |

#### Hawk.addDerivedAttribute ####

Add a new derived attribute to a Hawk instance.

Returns `void`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| name  | string | The name of the Hawk instance. |
| spec  | DerivedAttributeSpec | The details of the new derived attribute. |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| HawkInstanceNotFound | No Hawk instance exists with that name. |
| HawkInstanceNotRunning | The selected Hawk instance is not running. |
| InvalidDerivedAttributeSpec | The derived attribute specification is not valid. |

#### Hawk.removeDerivedAttribute ####

Remove a derived attribute from a Hawk instance.

Returns `void`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| name  | string | The name of the Hawk instance. |
| spec  | DerivedAttributeSpec | The details of the derived attribute to be removed. Only the first three fields of the spec need to be populated. |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| HawkInstanceNotFound | No Hawk instance exists with that name. |
| HawkInstanceNotRunning | The selected Hawk instance is not running. |

#### Hawk.listDerivedAttributes ####

Lists the derived attributes of a Hawk instance. Only the first three fields of the spec are currently populated.

Returns `list<DerivedAttributeSpec>`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| name  | string | The name of the Hawk instance. |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| HawkInstanceNotFound | No Hawk instance exists with that name. |
| HawkInstanceNotRunning | The selected Hawk instance is not running. |

#### Hawk.addIndexedAttribute ####

Add a new indexed attribute to a Hawk instance.

Returns `void`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| name  | string | The name of the Hawk instance. |
| spec  | IndexedAttributeSpec | The details of the new indexed attribute. |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| HawkInstanceNotFound | No Hawk instance exists with that name. |
| HawkInstanceNotRunning | The selected Hawk instance is not running. |
| InvalidIndexedAttributeSpec | The indexed attribute specification is not valid. |

#### Hawk.removeIndexedAttribute ####

Remove a indexed attribute from a Hawk instance.

Returns `void`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| name  | string | The name of the Hawk instance. |
| spec  | IndexedAttributeSpec | The details of the indexed attribute to be removed. |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| HawkInstanceNotFound | No Hawk instance exists with that name. |
| HawkInstanceNotRunning | The selected Hawk instance is not running. |

#### Hawk.listIndexedAttributes ####

Lists the indexed attributes of a Hawk instance.

Returns `list<IndexedAttributeSpec>`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| name  | string | The name of the Hawk instance. |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| HawkInstanceNotFound | No Hawk instance exists with that name. |
| HawkInstanceNotRunning | The selected Hawk instance is not running. |

#### Hawk.getModel ####

Returns the contents of one or more models indexed in a Hawk instance. Cross-model references are also resolved, and contained objects are always sent.

Returns `list<ModelElement>`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| name  | string | The name of the Hawk instance. |
| options  | HawkQueryOptions | Options to limit the contents to be sent. |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| HawkInstanceNotFound | No Hawk instance exists with that name. |
| HawkInstanceNotRunning | The selected Hawk instance is not running. |

#### Hawk.getRootElements ####

Returns the root objects of one or more models indexed in a Hawk instance. Node IDs are always sent, and contained objects are never sent.

Returns `list<ModelElement>`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| name  | string | The name of the Hawk instance. |
| options  | HawkQueryOptions | Options to limit the contents to be sent. |

#### Hawk.watchStateChanges ####

Returns subscription details to a queue of HawkStateEvents with notifications about changes in the indexer's state.

Returns `Subscription`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| name  | string | The name of the Hawk instance. |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| HawkInstanceNotFound | No Hawk instance exists with that name. |
| HawkInstanceNotRunning | The selected Hawk instance is not running. |

#### Hawk.watchModelChanges ####

Returns subscription details to a queue of HawkChangeEvents with notifications about changes to a set of indexed models.

Returns `Subscription`.  Takes these parameters:

| Name | Type | Documentation |
| ---- | ---- | ------------- |
| name  | string | The name of the Hawk instance. |
| repositoryUri  | string | The URI of the repository in which the model is contained. |
| filePath  | list<string> | The pattern(s) for the model file(s) in the repository. |
| clientID  | string | Unique client ID (used as suffix for the queue name). |
| durableEvents  | SubscriptionDurability | Durability of the subscription. |

May throw these exceptions:

| Name | Documentation |
| ---- | ------------- |
| HawkInstanceNotFound | No Hawk instance exists with that name. |
| HawkInstanceNotRunning | The selected Hawk instance is not running. |

## Entities


### AttributeSlot ###

Represents a slot that can store the value(s) of an attribute of a model element.

Inherits from: Slot.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| name (inherited) | string | The name of the model element property the value of which is stored in this slot. |
| value (optional) | SlotValue | Value of the slot (if set). |

Used in: ModelElement.

### CommitItem ###

Simplified entry within a commit of a repository.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| path | string | Path within the repository, using / as separator. |
| repoURL | string | URL of the repository. |
| revision | string | Unique identifier of the revision of the repository. |
| type | CommitItemChangeType | Type of change within the commit. |

Used in: HawkModelElementAdditionEvent, HawkModelElementRemovalEvent, HawkAttributeUpdateEvent, HawkAttributeRemovalEvent, HawkReferenceAdditionEvent, HawkReferenceRemovalEvent, HawkFileAdditionEvent, HawkFileRemovalEvent.

### ContainerSlot ###

Represents a slot that can store other model elements within a model element.

Inherits from: Slot.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| elements | list<ModelElement> | Contained elements for this slot. |
| name (inherited) | string | The name of the model element property the value of which is stored in this slot. |

Used in: ModelElement.

### Credentials ###

Credentials of the client in the target VCS.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| password | string | Password for logging into the VCS. |
| username | string | Username for logging into the VCS. |

Used in: Hawk.addRepository, Hawk.updateRepositoryCredentials.

### DerivedAttributeSpec ###

Used to configure Hawk's derived attributes (discussed in D5.3).


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| attributeName | string | The name of the derived attribute. |
| attributeType (optional) | string | The (primitive) type of the derived attribute. |
| derivationLanguage (optional) | string | The language used to express the derivation logic. |
| derivationLogic (optional) | string | An executable expression of the derivation logic in the language above. |
| isMany (optional) | bool | The multiplicity of the derived attribute. |
| isOrdered (optional) | bool | A flag specifying whether the order of the values of the derived attribute is significant (only makes sense when isMany=true). |
| isUnique (optional) | bool | A flag specifying whether the the values of the derived attribute are unique (only makes sense when isMany=true). |
| metamodelUri | string | The URI of the metamodel to which the derived attribute belongs. |
| typeName | string | The name of the type to which the derived attribute belongs. |

Used in: Hawk.addDerivedAttribute, Hawk.removeDerivedAttribute, Hawk.listDerivedAttributes.

### File ###

A file to be sent through the network.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| contents | binary | Sequence of bytes with the contents of the file. |
| name | string | Name of the file. |

Used in: Hawk.registerMetamodels.

### HawkAttributeRemovalEvent ###

Serialized form of an attribute removal event.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| attribute | string | Name of the attribute that was removed. |
| id | string | Identifier of the model element that was changed. |
| vcsItem | CommitItem | Entry within the commit that produced the changes. |

Used in: HawkChangeEvent.

### HawkAttributeUpdateEvent ###

Serialized form of an attribute update event.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| attribute | string | Name of the attribute that was changed. |
| id | string | Identifier of the model element that was changed. |
| value | SlotValue | New value for the attribute. |
| vcsItem | CommitItem | Entry within the commit that produced the changes. |

Used in: HawkChangeEvent.

### HawkChangeEvent ###

Serialized form of a change in the indexed models of a Hawk instance.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| fileAddition | HawkFileAdditionEvent | A file was added. |
| fileRemoval | HawkFileRemovalEvent | A file was removed. |
| modelElementAddition | HawkModelElementAdditionEvent | A model element was added. |
| modelElementAttributeRemoval | HawkAttributeRemovalEvent | An attribute was removed. |
| modelElementAttributeUpdate | HawkAttributeUpdateEvent | An attribute was updated. |
| modelElementRemoval | HawkModelElementRemovalEvent | A model element was removed. |
| referenceAddition | HawkReferenceAdditionEvent | A reference was added. |
| referenceRemoval | HawkReferenceRemovalEvent | A reference was removed. |
| syncEnd | HawkSynchronizationEndEvent | Synchronization ended. |
| syncStart | HawkSynchronizationStartEvent | Synchronization started. |



### HawkFileAdditionEvent ###

Serialized form of a file addition event.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| vcsItem | CommitItem | Reference to file that was added, including VCS metadata. |

Used in: HawkChangeEvent.

### HawkFileRemovalEvent ###

A file was removed.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| vcsItem | CommitItem | Reference to file that was removed, including VCS metadata. |

Used in: HawkChangeEvent.

### HawkInstance ###

Status of a Hawk instance.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| message | string | Last info message from the instance. |
| name | string | The name of the instance. |
| state | HawkState | Current state of the instance. |

Used in: Hawk.listInstances.

### HawkModelElementAdditionEvent ###

Serialized form of a model element addition event.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| id | string | Identifier of the model element that was added. |
| metamodelURI | string | Metamodel URI of the type of the model element. |
| typeName | string | Name of the type of the model element. |
| vcsItem | CommitItem | Entry within the commit that produced the changes. |

Used in: HawkChangeEvent.

### HawkModelElementRemovalEvent ###

Serialized form of a model element removal event.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| id | string | Identifier of the model element that was removed. |
| vcsItem | CommitItem | Entry within the commit that produced the changes. |

Used in: HawkChangeEvent.

### HawkPlugin ###

Details about a Hawk plugin installed on the server.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| category | HawkPluginCategory | Plugin category. |
| description | string | Human-friendly description. |
| name | string | Unique identifier. |

Used in: Hawk.listPluginDetails.

### HawkQueryOptions ###

Options for a Hawk query.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| defaultNamespaces (optional) | string | The default namespaces to be used to resolve ambiguous unqualified types. |
| effectiveMetamodelExcludes (optional) | map<string,map<string,set<string>>> | If set and not empty, the mentioned metamodels, types and features will not be fetched. The string '*' can be used to refer to all types within a metamodel or all fields within a type. |
| effectiveMetamodelIncludes (optional) | map<string,map<string,set<string>>> | If set and not empty, only the specified metamodels, types and features will be fetched. Otherwise, everything that is not excluded will be fetched. The string '*' can be used to refer to all types within a metamodel or all fields within a type. |
| filePatterns (optional) | list<string> | The file patterns for the query (e.g. *.uml). |
| includeAttributes (optional) | bool | Whether to include attributes (true) or not (false) in model element results. |
| includeContained (optional) | bool | Whether to include all the child elements of the model element results (true) or not (false). |
| includeDerived (optional) | bool | Whether to include derived attributes (true) or not (false) in model element results. |
| includeNodeIDs (optional) | bool | Whether to include node IDs (true) or not (false) in model element results. |
| includeReferences (optional) | bool | Whether to include references (true) or not (false) in model element results. |
| repositoryPattern (optional) | string | The repository for the query (or * for all repositories). |

Used in: Hawk.query, Hawk.timedQuery, Hawk.resolveProxies, Hawk.asyncQuery, Hawk.getModel, Hawk.getRootElements.

### HawkReferenceAdditionEvent ###

Serialized form of a reference addition event.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| refName | string | Name of the reference that was added. |
| sourceId | string | Identifier of the source model element. |
| targetId | string | Identifier of the target model element. |
| vcsItem | CommitItem | Entry within the commit that produced the changes. |

Used in: HawkChangeEvent.

### HawkReferenceRemovalEvent ###

Serialized form of a reference removal event.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| refName | string | Name of the reference that was removed. |
| sourceId | string | Identifier of the source model element. |
| targetId | string | Identifier of the target model element. |
| vcsItem | CommitItem | Entry within the commit that produced the changes. |

Used in: HawkChangeEvent.

### HawkStateEvent ###

Serialized form of a change in the state of a Hawk instance.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| message | string | Short message about the current status of the server. |
| state | HawkState | State of the Hawk instance. |
| timestamp | i64 | Timestamp for this state change. |



### HawkSynchronizationEndEvent ###

Serialized form of a sync end event.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| timestampNanos | i64 | Local timestamp, measured in nanoseconds. Only meant to be used to compute synchronization cost. |

Used in: HawkChangeEvent.

### HawkSynchronizationStartEvent ###

Serialized form of a sync start event.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| timestampNanos | i64 | Local timestamp, measured in nanoseconds. Only meant to be used to compute synchronization cost. |

Used in: HawkChangeEvent.

### IndexedAttributeSpec ###

Used to configure Hawk's indexed attributes (discussed in D5.3).


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| attributeName | string | The name of the indexed attribute. |
| metamodelUri | string | The URI of the metamodel to which the indexed attribute belongs. |
| typeName | string | The name of the type to which the indexed attribute belongs. |

Used in: Hawk.addIndexedAttribute, Hawk.removeIndexedAttribute, Hawk.listIndexedAttributes.

### InvalidModelSpec ###

The model specification is not valid: the model or the metamodels are inaccessible or invalid.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| reason | string | Reason for the spec not being valid. |
| spec | ModelSpec | A copy of the invalid model specification. |



### InvalidTransformation ###

The transformation is not valid: it is unparsable or inconsistent.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| location | string | Location of the problem, if applicable. Usually a combination of line and column numbers. |
| reason | string | Reason for the transformation not being valid. |



### MetamodelParserDetails ###

Details for a metamodel registered in a Hawk instance.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| fileExtensions | set<string> | Recognised file extensions, starting with '.'. |
| identifier | string | Type identifier. |

Used in: Hawk.listMetamodelParsers.

### MixedReference ###

Represents a reference to a model element: it can be an identifier or a position.
Only used when the same ReferenceSlot has both identifier-based and position-based references.
This may be the case if we are retrieving a subset of the model which has references
between its elements and with elements outside the subset at the same time.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| id | string | Identifier-based reference to a model element. |
| position | i32 | Position-based reference to a model element. |

Used in: ReferenceSlot.

### ModelElement ###

Represents a model element.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| attributes (optional) | list<AttributeSlot> | Slots holding the values of the model element's attributes, if any have been set. |
| containers (optional) | list<ContainerSlot> | Slots holding contained model elements, if any have been set. |
| file (optional) | string | Name of the file to which the element belongs (not set if equal to that of the previous model element). |
| id (optional) | string | Unique ID of the model element (not set if using position-based references). |
| metamodelUri (optional) | string | URI of the metamodel to which the type of the element belongs (not set if equal to that of the previous model element). |
| references (optional) | list<ReferenceSlot> | Slots holding the values of the model element's references, if any have been set. |
| repositoryURL (optional) | string | URI of the repository to which the element belongs (not set if equal to that of the previous model element). |
| typeName (optional) | string | Name of the type that the model element is an instance of (not set if equal to that of the previous model element). |

Used in: Hawk.resolveProxies, Hawk.getModel, Hawk.getRootElements, ContainerSlot, QueryResult.

### ModelElementType ###

Represents a type of model element.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| attributes (optional) | list<SlotMetadata> | Metadata for the attribute slots. |
| id | string | Unique ID of the model element type. |
| metamodelUri | string | URI of the metamodel to which the type belongs. |
| references (optional) | list<SlotMetadata> | Metadata for the reference slots. |
| typeName | string | Name of the type. |

Used in: QueryResult.

### ModelSpec ###

Captures information about source/target models of ATL transformations.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| metamodelUris | list<string> | The URIs of the metamodels to which elements of the model conform. |
| uri | string | The URI from which the model will be loaded or to which it will be persisted. |

Used in: InvalidModelSpec.

### QueryReport ###

Report from a query, including execution time and any other information we may consider later.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| isCancelled | bool | Was the query cancelled?. |
| result | QueryResult | Result of the query. |
| wallMillis | i64 | Wall time on the server in milliseconds. |

Used in: Hawk.timedQuery, Hawk.fetchAsyncQueryResults.

### QueryResult ###

Union type for a scalar value, a reference to a model element, a heterogeneous
list or a string/value map. Query results may return all types of results, so we need
to stay flexible.

Inherits from: Value.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| vBoolean (inherited) | bool | Boolean (true/false) value. |
| vByte (inherited) | i8 | 8-bit signed integer value. |
| vDouble (inherited) | double | 64-bit floating point value. |
| vInteger (inherited) | i32 | 32-bit signed integer value. |
| vList | list<QueryResult> | Nested list of query results. |
| vLong (inherited) | i64 | 64-bit signed integer value. |
| vMap | map<string,QueryResult> | Map between query results. |
| vModelElement | ModelElement | Encoded model element. |
| vModelElementType | ModelElementType | Encoded model element type. |
| vShort (inherited) | i16 | 16-bit signed integer value. |
| vString (inherited) | string | Sequence of UTF8 characters. |

Used in: Hawk.query, QueryResult, QueryReport, QueryResultMap.

### ReferenceSlot ###

Represents a slot that can store the value(s) of a reference  of a model element.
References can be expressed as positions within a result tree (using pre-order traversal)
or IDs. id, ids, position, positions and mixed are all mutually exclusive. At least one position
or one ID must be given.

Inherits from: Slot.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| id (optional) | string | Unique identifier of the referenced element (if there is only one ID based reference in this slot). |
| ids (optional) | list<string> | Unique identifiers of the referenced elements (if more than one). |
| mixed (optional) | list<MixedReference> | Mix of identifier- and position-bsaed references (if there is at least one position and one ID). |
| name (inherited) | string | The name of the model element property the value of which is stored in this slot. |
| position (optional) | i32 | Position of the referenced element (if there is only one position-based reference in this slot). |
| positions (optional) | list<i32> | Positions of the referenced elements (if more than one). |

Used in: ModelElement.

### Repository ###

Entity that represents a model repository.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| isFrozen (optional) | bool | True if the repository is frozen, false otherwise. |
| type | string | The type of repository. |
| uri | string | The URI to the repository. |

Used in: Hawk.addRepository, Hawk.listRepositories.

### Slot ###

Represents a slot that can store the value(s) of a property of a model element.

Inherited by: AttributeSlot, ReferenceSlot, ContainerSlot.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| name | string | The name of the model element property the value of which is stored in this slot. |



### SlotMetadata ###

Represents the metadata of a slot in a model element type.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| isMany | bool | True if this slot holds a collection of values instead of a single value. |
| isOrdered | bool | True if the values in this slot are ordered. |
| isUnique | bool | True if the value of this slot must be unique within its containing model. |
| name | string | The name of the model element property that is stored in this slot. |
| type | string | The type of the values in this slot. |

Used in: ModelElementType.

### SlotValue ###

Union type for a single scalar value, a homogeneous collection of scalar values, or a collection of collections.

Inherits from: Value.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| vBoolean (inherited) | bool | Boolean (true/false) value. |
| vBooleans | list<bool> | List of true/false values. |
| vByte (inherited) | i8 | 8-bit signed integer value. |
| vBytes | binary | List of 8-bit signed integers. |
| vDouble (inherited) | double | 64-bit floating point value. |
| vDoubles | list<double> | List of 64-bit floating point values. |
| vInteger (inherited) | i32 | 32-bit signed integer value. |
| vIntegers | list<i32> | List of 32-bit signed integers. |
| vLists | list<SlotValue> | List of lists. |
| vLong (inherited) | i64 | 64-bit signed integer value. |
| vLongs | list<i64> | List of 64-bit signed integers. |
| vShort (inherited) | i16 | 16-bit signed integer value. |
| vShorts | list<i16> | List of 16-bit signed integers. |
| vString (inherited) | string | Sequence of UTF8 characters. |
| vStrings | list<string> | List of sequences of UTF8 characters. |

Used in: HawkAttributeUpdateEvent, AttributeSlot, SlotValue.

### Subscription ###

Details about a subscription to a topic queue.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| host | string | Host name of the message queue server. |
| port | i32 | Port in which the message queue server is listening. |
| queueAddress | string | Address of the topic queue. |
| queueName | string | Name of the topic queue. |
| sslRequired | bool | Whether SSL is required or not. |

Used in: Hawk.watchStateChanges, Hawk.watchModelChanges.

### UserProfile ###

Minimal details about registered users.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| admin | bool | Whether the user has admin rights (i.e. so that they can create new users, change the status of admin users etc). |
| realName | string | The real name of the user. |

Used in: Users.createUser, Users.updateProfile.

### Value ###

Union type for a single scalar value.

Inherited by: QueryResult, SlotValue.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| vBoolean | bool | Boolean (true/false) value. |
| vByte | i8 | 8-bit signed integer value. |
| vDouble | double | 64-bit floating point value. |
| vInteger | i32 | 32-bit signed integer value. |
| vLong | i64 | 64-bit signed integer value. |
| vShort | i16 | 16-bit signed integer value. |
| vString | string | Sequence of UTF8 characters. |



## Enumerations

### CommitItemChangeType ###

Type of change within a commit.

| Name | Documentation |
| ---- | ------------- |
| ADDED | File was added. |
| DELETED | File was removed. |
| REPLACED | File was removed. |
| UNKNOWN | Unknown type of change. |
| UPDATED | File was updated. |

### HawkPluginCategory ###

Hawk plugin categories.

| Name | Documentation |
| ---- | ------------- |
| BACKEND |  |
| GRAPH_CHANGE_LISTENER |  |
| INDEX_FACTORY |  |
| METAMODEL_INTROSPECTOR |  |
| METAMODEL_RESOURCE_FACTORY |  |
| METAMODEL_UPDATER |  |
| MODEL_RESOURCE_FACTORY |  |
| MODEL_UPDATER |  |
| QUERY_ENGINE |  |
| VCS_MANAGER |  |

### HawkState ###

One of the states that a Hawk instance can be in.

| Name | Documentation |
| ---- | ------------- |
| RUNNING | The instance is running and monitoring the indexed locations. |
| STOPPED | The instance is stopped and is not monitoring any indexed locations. |
| UPDATING | The instance is updating its contents from the indexed locations. |

### SubscriptionDurability ###

Durability of a subscription.

| Name | Documentation |
| ---- | ------------- |
| DEFAULT | Subscription survives client disconnections but not server restarts. |
| DURABLE | Subscription survives client disconnections and server restarts. |
| TEMPORARY | Subscription removed after disconnecting. |
## Exceptions


### FailedQuery ###
The specified query failed to complete its execution.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| reason | string | Reason for the query failing to complete its execution. |

Used in: Hawk.query, Hawk.timedQuery, Hawk.fetchAsyncQueryResults.

### HawkFactoryNotFound ###
No Hawk factory exists with that name.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| factoryName (optional) | string | Name provided by the client. |

Used in: Hawk.createInstance.

### HawkInstanceNotFound ###
No Hawk instance exists with that name.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| instanceName (optional) | string | Name provided by the client. |

Used in: Hawk.removeInstance, Hawk.startInstance, Hawk.stopInstance, Hawk.syncInstance, Hawk.registerMetamodels, Hawk.unregisterMetamodels, Hawk.listMetamodels, Hawk.listTypeNames, Hawk.listAttributeNames, Hawk.listMetamodelParsers, Hawk.query, Hawk.timedQuery, Hawk.resolveProxies, Hawk.asyncQuery, Hawk.addRepository, Hawk.isFrozen, Hawk.setFrozen, Hawk.removeRepository, Hawk.updateRepositoryCredentials, Hawk.listRepositories, Hawk.listFiles, Hawk.configurePolling, Hawk.addDerivedAttribute, Hawk.removeDerivedAttribute, Hawk.listDerivedAttributes, Hawk.addIndexedAttribute, Hawk.removeIndexedAttribute, Hawk.listIndexedAttributes, Hawk.getModel, Hawk.watchStateChanges, Hawk.watchModelChanges.

### HawkInstanceNotRunning ###
The selected Hawk instance is not running.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| instanceName (optional) | string | Name provided by the client. |

Used in: Hawk.stopInstance, Hawk.syncInstance, Hawk.registerMetamodels, Hawk.unregisterMetamodels, Hawk.listMetamodels, Hawk.listTypeNames, Hawk.listAttributeNames, Hawk.listMetamodelParsers, Hawk.query, Hawk.timedQuery, Hawk.resolveProxies, Hawk.asyncQuery, Hawk.addRepository, Hawk.isFrozen, Hawk.setFrozen, Hawk.removeRepository, Hawk.updateRepositoryCredentials, Hawk.listRepositories, Hawk.listFiles, Hawk.configurePolling, Hawk.addDerivedAttribute, Hawk.removeDerivedAttribute, Hawk.listDerivedAttributes, Hawk.addIndexedAttribute, Hawk.removeIndexedAttribute, Hawk.listIndexedAttributes, Hawk.getModel, Hawk.watchStateChanges, Hawk.watchModelChanges.

### HawkMetamodelNotFound ###
The selected Hawk instance does not have a metamodel with that URI.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| metamodelURI (optional) | string | URI provided by the client. |

Used in: Hawk.listTypeNames, Hawk.listAttributeNames.

### HawkTypeNotFound ###
The selected Hawk instance does not have a type with that name.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| typeName (optional) | string | Name provided by the client. |

Used in: Hawk.listAttributeNames.

### InvalidDerivedAttributeSpec ###
The derived attribute specification is not valid.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| reason | string | Reason for the spec not being valid. |

Used in: Hawk.addDerivedAttribute.

### InvalidIndexedAttributeSpec ###
The indexed attribute specification is not valid.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| reason | string | Reason for the spec not being valid. |

Used in: Hawk.addIndexedAttribute.

### InvalidMetamodel ###
The provided metamodel is not valid (e.g. unparsable or inconsistent).


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| reason | string | Reason for the metamodel not being valid. |

Used in: Hawk.registerMetamodels.

### InvalidPollingConfiguration ###
The polling configuration is not valid.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| reason | string | Reason for the spec not being valid. |

Used in: Hawk.configurePolling.

### InvalidQuery ###
The specified query is not valid.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| reason | string | Reason for the query not being valid. |

Used in: Hawk.query, Hawk.timedQuery, Hawk.asyncQuery, Hawk.cancelAsyncQuery, Hawk.fetchAsyncQueryResults.

### UnknownQueryLanguage ###
The specified query language is not supported by the operation.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| queryLanguage (optional) | string | Query language requested by the client. |

Used in: Hawk.query, Hawk.timedQuery, Hawk.asyncQuery.

### UnknownRepositoryType ###
The specified repository type is not supported by the operation.


| Name | Type | Documentation |
| ---- | ---- | ------------- |
| repositoryType (optional) | string | Repository type provided by the client. |

Used in: Hawk.addRepository.

### UserExists ###
The specified username already exists.

No fields for this entity.

Used in: Users.createUser.

### UserNotFound ###
The specified username does not exist.

No fields for this entity.

Used in: Users.updateProfile, Users.updatePassword, Users.deleteUser.

### VCSAuthenticationFailed ###
The client failed to prove its identity in the VCS.

No fields for this entity.

Used in: Hawk.addRepository.

---

This file was automatically generated by [Ecore2Thrift](https://github.com/agarciadom/ecore2thrift).
