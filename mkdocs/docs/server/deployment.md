Initial setup
-------------

To run the Hawk server, download the latest `hawk-server-*.zip` file for your operating system and architecture of choice from the Hawk [download page](https://eclipse.org/hawk/), and unpack it. Note that `-nogpl-` releases do not include GPL-licensed components: if you want them in your server, you will have to build it yourself.

Make any relevant changes to the `hawk-server.ini` file, and then run the `run-server.sh` script from Linux, or simply the provided `hawk-server` binary from Mac or Windows.

If everything goes well, you should see this message:

```
Welcome to the Hawk Server!
List available commands with 'hserverHelp'.
Stop the server with 'shutdown' and then 'close'.

osgi> 
```

You may now use the Thrift APIs as normal. If you need to make any tweaks, continue reading!

.ini options
------------

You will notice that the `.ini` file has quite a few different options defined, in addition to the JVM options defined with `-vmargs`. We will analyze them in this section.

* `-console` allows us to use the OSGi console to manage Hawk instances.
* `-consoleLog` plugs Eclipse logging into the console, for following what is going with the server.
* `-Dartemis.security.enabled=false` disables the Shiro security realm for the embedded Artemis server. Production environments should set this to `true`.
* `-Dhawk.artemis.host=localhost` has Artemis listening only on 127.0.0.1. You should change this to the IP address or hostname of the network interface that you want Artemis to listen on. Alternatively, you can have Artemis listening in all addresses (see `-Dhawk.artemis.listenAll` below).
* `-Dhawk.artemis.port=61616` has Artemis listening on port 61616 in the CORE and STOMP protocols.
* `-Dhawk.artemis.listenAll=false` prevents Artemis from listening on all addresses. You can set this to `true` and ignore `hawk.artemis.host`.
* `-Dhawk.artemis.sslEnabled=false` disables HTTPS on Artemis. If you enable SSL, you will need to check the "Enabling HTTPS" section further below!
* `-Dhawk.tcp.port=2080` enables the TCP server for only the Hawk API, and not the Users management one. This API is unsecured, so do this at your own risk. For production environments, you should remove this line.
* `-Dhawk.tcp.thriftProtocol=TUPLE` changes the Thrift protocol (encoding) that should be used for the TCP endpoint.
* `-Dorg.eclipse.equinox.http.jetty.customizer.class=org.eclipse.hawk.service.server.gzip.Customizer` is needed for the automated gzip compression integrated with Thrift.
* `-Dorg.osgi.service.http.port=8080` sets the HTTP port for the APIs to 8080.
* `-Dorg.osgi.service.http.port.secure=8443` sets the HTTPS port for the APIs to 8443.
* `-Dosgi.noShutdown=true` is needed for the server to stay running.
* `-Dsvnkit.library.gnome-keyring.enabled=false` is required to work around a bug in the integration of the GNOME keyring in recent Eclipse releases.
* `-eclipse.keyring` and `-eclipse.password` are the paths to the keyring and keyring password files which store the VCS credentials Hawk needs to access password-protected SVN repositories. (For Git repositories, you are assumed to keep your own clone and do any periodic pulling yourself.)
* `-XX:+UseG1GC` (part of `-vmargs`) improves garbage collection in OrientDB and Neo4j.
* `-XX:+UseStringDeduplication` (part of `-vmargs` as well) noticeably reduces memory use in OrientDB.

Ports
---

These are the default ports that a Hawk server uses:

* 2080: Hawk raw TCP API, available by default (unsecured: see above for how to disable it)
* 8080: Hawk HTTP API, available by default (optionally secured: see below)
* 8443: Hawk HTTPS API, if enabled (optionally secured: see below)
* 61616: Artemis push notifications for Hawk index status updates (optionally secured / encrypted: see below)

Concerns for production environments
------------------------------------

One important detail for production environments is turning on security. This is disabled by default to help with testing and initial evaluations, but it can be enabled by running the server once, shutting it down and then editing the `shiro.ini` file appropriately (relevant sections include comments on what to do) and switching `artemis.security.enabled` to `true` in the `hawk-server.ini` file. The MONDO server uses an embedded MapDB database, which is managed through the Users [[Thrift API]]. Once security is enabled, all Thrift APIs and all external (not in-VM) Artemis connections become password-protected.

If you enable security, you will want to ensure that `-Dhawk.tcp.port` is not present in the `hawk-server.ini` file, since the Hawk TCP port does not support security for the sake of raw performance.

If you are deploying this across a network, you will need to edit the `hawk-server.ini` file and customize the `hawk.artemis.host` line to the host that you want the Artemis server to listen to. This should be the IP address or hostname of the MONDO server in the network, normally. The Thrift API uses this hostname as well in its replies to the `watchModelChanges` operation in the Hawk API.

Additionally, if the server IP is dynamic but has a consistent DNS name (e.g. an Amazon VM + a dynamic DNS provider), we recommend setting `hawk.artemis.listenAll` to `true` (so the Artemis server will keep listening on all interfaces, even if the IP address changes) and using the DNS name for `hawk.artemis.host` instead of a literal IP address.

You may also want to customize the CORS header (`Access-Control-Allow-Origin`) to limit the domains that can invoke the Hawk API. This can be done by adding `-Dhawk.http.allowOrigin=yourOrigin` to the `.ini` options.

Finally, production environments should enable and enforce SSL as well, since plain HTTP is insecure. The Linux products include a shell script that generates simple self-signed key/trust stores and indicates which Java system properties should be set on the server and the client.

Secure storage of VCS credentials
---------------------------------

The server hosts a copy of the Hawk model indexer, which may need to access remote Git and Subversion repositories. To access password-protected repositories, the server will need to store the proper credentials in a secure way that will not expose them to other users in the same machine. To achieve this goal, the MONDO server uses the Eclipse secure storage facilities to save the password in an encrypted form. Users need to prepare the secure storage by following these two steps:

1. The secure store must be placed in a place no other program will try to access concurrently.
   This can be done by editing the `hawk-server.ini` server configuration file and adding this:
   ```
   -eclipse.keyring
   /path/to/keyringfile
   ```
   That path should be only readable by the user running the server, for added security.

2. An encryption password must be set. For Windows and Mac, the available OS integration should
   be enough. For Linux environments, two lines have to be added at the beginning of the
   `hawk-server.ini` file, specifying the path to a password file with:
   ```
   -eclipse.password
   /path/to/passwordfile.
   ```
   On Linux, creating a password file from 100 bytes of random data that is only readable by
   the current user can be done with these commands:
   ```
   $ head -c 100 /dev/random | base64 > /path/to/password
   $ chmod 400 /path/to/password
   ```

The server tests on startup that the secure store has been set properly, warning users if encryption is not available and urging them to revise their setup.

Setting up SSL certificates for the server
------------------------------------------

SSL is handled through standard Java keystore (`.jks`) files. To produce a keystore with some self-signed certificates, you could use the `generate-ssl-certs.sh` script included in the Linux distribution, or run these commands from other operating systems (replace CN, OU and so forth with the appropriate values):

```
keytool -genkey -keystore hawk-server-keystore.jks -storepass secureexample -keypass secureexample -dname "CN=localhost, OU=Artemis, O=ActiveMQ, L=AMQ, S=AMQ, C=AMQ" -keyalg RSA
keytool -export -keystore hawk-server-keystore.jks -file hawk-jks.cer -storepass secureexample
keytool -import -keystore hawk-client-truststore.jks -file hawk-jks.cer -storepass secureexample -keypass secureexample -noprompt
```

Once you have your .jks, on the client .ini you'll need to set:

```
-Djavax.net.ssl.trustStore=path/to/client-truststore.jks
-Djavax.net.ssl.trustStorePassword=secureexample
```

On the server .ini, you'll need to enable SSL and tell Jetty and Artemis about your KeyStore:

```
-Dorg.eclipse.equinox.http.jetty.https.enabled=true
-Dhawk.artemis.sslEnabled=true
-Dorg.eclipse.equinox.http.jetty.ssl.keystore=path/to/server-keystore.jks
-Djavax.net.ssl.keyStore=path/to/server-keystore.jks
```

You'll be prompted for the key store password three times: two by Jetty and once by the Artemis server. If you don't want these prompts, you could use these properties, but using them is *UNSAFE*, as another user in the same machine could retrieve these passwords from your process manager:

```
-Djavax.net.ssl.keyStorePassword=secureexample
-Dorg.eclipse.equinox.http.jetty.ssl.keypassword=secureexample
-Dorg.eclipse.equinox.http.jetty.ssl.password=secureexample
```
