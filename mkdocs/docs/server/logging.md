Logging in Hawk is done through the [Logback](https://logback.qos.ch/) library. The specific `logback.xml` file is part of the `org.eclipse.hawk.service.server.logback` plugin fragment. If you need to edit it, it is located in the `plugins/org.eclipse.hawk.service.server.logback_<HAWK RELEASE>` folder from the main directory of the server.

A typical configuration with Hawk logging at the DEBUG level, with time-based rolling and all messages going to the `hawk.log` file would look as follows:

```xml
<configuration>
	<appender name="STDOUT" class="ch.qos.logback.core.ConsoleAppender">
		<layout class="ch.qos.logback.classic.PatternLayout">
			<Pattern>
				%d{yyyy-MM-dd HH:mm:ss} [%thread] %-5level %logger{36} -
				%msg%n
			</Pattern>
		</layout>
	</appender>

	<appender name="FILE"
		class="ch.qos.logback.core.rolling.RollingFileAppender">
		<file>hawk.log</file>
		<encoder class="ch.qos.logback.classic.encoder.PatternLayoutEncoder">
			<Pattern>
				%d{yyyy-MM-dd HH:mm:ss} [%thread] %-5level %logger{36} - %msg%n
			</Pattern>
		</encoder>

		<rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
			<!-- rollover daily -->
			<fileNamePattern>mylog-%d{yyyy-MM-dd}.%i.txt</fileNamePattern>
			<maxHistory>60</maxHistory>
		</rollingPolicy>
	</appender>

	<logger name="org.eclipse.jetty" level="warn" additivity="false">
		<appender-ref ref="STDOUT" />
	</logger>

	<logger name="ch.qos.logback" level="error" additivity="false">
		<appender-ref ref="STDOUT" />
	</logger>

	<logger name="org.apache.shiro" level="error" additivity="false">
		<appender-ref ref="STDOUT" />
	</logger>

	<!-- Change to "error" if Hawk produces too many messages for you -->
	<logger name="org.eclipse.hawk" level="debug" additivity="false">
		<appender-ref ref="STDOUT" />
		<appender-ref ref="FILE" />
	</logger>

	<root level="debug">
		<appender-ref ref="STDOUT" />
	</root>
</configuration>
```
