# Eclipse Hawk™

Eclipse Hawk is a model indexing solution that can take models written with various technologies and turn them into graph databases, for easier and faster querying.

Hawk is licensed under the [Eclipse Public License 2.0](https://www.eclipse.org/legal/epl-2.0/), with the [GNU GPL 3.0](https://www.gnu.org/licenses/gpl-3.0.en.html) as secondary license.

!!! question "Any questions?"

    Check the other sections on the left for how to get started and use Hawk. If you cannot find an answer there, feel free to ask at the [official forum in Eclipse.org](https://www.eclipse.org/forums/index.php/sf/thread/442/sub/0/?SQ=c1bf1ac983010df5037be417b0a8d5a1).

## Eclipse update sites

The core components of Hawk, the OrientDB / Greycat / SQLite backends, and the Thrift API clients can be installed from one of these Eclipse update sites:

| Site | Location |
| - | - |
| Stable | <https://download.eclipse.org/hawk/{{ stable.version }}/updates/> |
| Interim | <https://download.eclipse.org/hawk/{{ interim.version }}/updates/> |

If you are developing a custom Hawk server, you will find the Hawk server components in these update sites:

| Site | Location |
| - | - |
| Stable | <https://download.eclipse.org/hawk/{{ stable.version }}/server/> |
| Interim | <https://download.eclipse.org/hawk/{{ interim.version }}/server/> |

## Plain libraries

Many of the Eclipse Hawk components are available via Maven Central under the `org.eclipse.hawk` group ID:

| Site   | Repository                                                            | Group ID | Version |
| -      | -                                                                     | - | - |
| Stable | [Maven Central](https://search.maven.org/search?q=g:org.eclipse.hawk) | org.eclipse.hawk | {{ stable.version }} |
| Interim | [OSSRH](https://oss.sonatype.org/#nexus-search;gav~org.eclipse.hawk~~{{ interim.version }}-SNAPSHOT~~) | org.eclipse.hawk | {{ interim.version }}-SNAPSHOT |

## Thrift API libraries

There are [Apache Thrift](http://thrift.apache.org/) client libraries targeting C++, Java, JavaScript, and Python for talking with a Hawk server over its Thrift API. The Java libraries are available as Maven artefacts (see above). The C++ and JavaScript libraries can be downloaded from the links below.

### C++ libraries

| Site   | Location                                                                                            |
| -      | -                                                                                                   |
| Stable | <http://download.eclipse.org/hawk/{{ stable.version }}/hawk-thrift-cpp-{{ stable.version }}.tar.gz> |
| Interim | <http://download.eclipse.org/hawk/{{ interim.version }}/hawk-thrift-cpp-{{ interim.version }}.tar.gz> |

### JavaScript libraries

| Site   | Location                                                                                            |
| -      | -                                                                                                   |
| Stable | <http://download.eclipse.org/hawk/{{ stable.version }}/hawk-thrift-js-{{ stable.version }}.tar.gz> |
| Interim | <http://download.eclipse.org/hawk/{{ interim.version }}/hawk-thrift-js-{{ interim.version }}.tar.gz> |

### Python libraries

| Site   | Location                                                                                            |
| -      | -                                                                                                   |
| Stable | <http://download.eclipse.org/hawk/{{ stable.version }}/hawk-thrift-py-{{ stable.version }}.tar.gz> |
| Interim | <http://download.eclipse.org/hawk/{{ interim.version }}/hawk-thrift-py-{{ interim.version }}.tar.gz> |

## Firewall-friendly artefacts

For environments with corporate firewalls, the zipped update sites, zipped source code, and prebuilt CLI/server products for Linux, MacOS and Windows are available from the download folders:

| Folder | Location |
| -      | -        |
| Stable | <https://download.eclipse.org/hawk/{{ stable.version }}/> |
| Interim | <https://download.eclipse.org/hawk/{{ interim.version }}/> |

## Docker images

Docker images for the Hawk Server are available from the [hawk-docker](https://gitlab.com/hawklabs/hawk-docker/) project at Hawk Labs.
These Docker images are rebuilt at least once a week, or whenever there are new changes in Hawk.

## Source code

To access the source code, clone the Git repository for Hawk with your preferred client from:

    https://gitlab.eclipse.org/eclipse/hawk/hawk.git

Committers will use a different URL:

    git@gitlab.eclipse.org:eclipse/hawk/hawk.git

You can also read the code through your browser from the [Eclipse Gitlab instance](https://gitlab.eclipse.org/eclipse/hawk/hawk/-/tree/master) (which allows for archive downloads).

## Older versions

Downloads for older versions are archived at Eclipse.org:

<ul>
{% for version in older %}
<li><a href="http://download.eclipse.org/hawk/{{ version }}/">{{ version }}</a></li>
{% endfor %}
</ul>
