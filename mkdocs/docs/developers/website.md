The website for Eclipse Hawk is written in [MkDocs](https://www.mkdocs.org).

To work on the website, clone the [Git repository](https://gitlab.eclipse.org/eclipse/hawk/hawk-website) with your Eclipse credentials, and follow the instructions in the included `README.md` file.
