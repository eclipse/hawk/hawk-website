Hawk can be reused as a library in a regular Java application, outside OSGi. Non-OSGi developers normally use Maven or a Maven-compatible build system (e.g. Ivy or SBT), rather than Tycho. To make it easier for these developers, Hawk provides a parallel hierarchy of `pom-plain.xml` files that can be used to build Hawk with plain Maven (`pom.xml` files are reserved for Tycho).

Not all Hawk modules are available through this build, as they may rely on OSGi (e.g. `org.eclipse.hawk.modelio`) or require downloading many external dependencies. `.feature`, `.dependencies` and `.tests` projects are not included either, as they are OSGi-specific. For that reason, this build should only be used for distribution, and not for regular development.

To build with regular Maven, run `mvn -f pom-plain.xml install` from the root of the repository to compile the artifacts and install them into the local Maven repository, so they can be used in other Maven builds.
