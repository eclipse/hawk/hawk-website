These instructions are from a clean download of an Eclipse Modelling distribution and include all optional dependencies.

1. Clone this Git repository on your Eclipse instance (e.g. using `git clone` or EGit) and import all projects except for those inside the `server` folder into the workspace (File > Import > Existing Projects into Workspace).
1. Open the `org.eclipse.hawk.targetplatform/org.eclipse.hawk.targetplatform.target` file, wait for the target definition to be resolved and click on `Set as Target Platform`.
1. Install [IvyDE](https://ant.apache.org/ivy/ivyde/) into your Eclipse instance, right click on `org.eclipse.hawk.neo4j-v2.dependencies` and use "Ivy > Retrieve 'dependencies'". The libraries should appear within `Referenced Libraries`. Do the same for these other projects:
    * `org.eclipse.hawk.greycat`
    * `org.eclipse.hawk.localfolder`
    * `org.eclipse.hawk.orientdb`
    * `org.eclipse.hawk.sqlite`
1. Force a full rebuild with `Project > Clean... > Clean all projects` if you still have errors.

After all these steps, you should have a working version of Hawk with all optional dependencies and no errors. You can now right-click on any Hawk project and select "Run as... > Eclipse Application" to open a nested Eclipse with the Hawk GUI running inside it.
