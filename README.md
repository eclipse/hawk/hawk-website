# Hawk website

This repository hosts the [MkDocs](https://www.mkdocs.org) source code for the Eclipse Hawk website.

## Development

To work on the website, go into the `mkdocs` folder and set up a Python 3 [virtualenv](https://virtualenv.pypa.io/en/latest/) environment and install the dependencies through `pip`:

    virtualenv -p python3 env
    pip install -r requirements.txt

The `virtualenv` environment will remain active until you terminate that console session. If you want to continue working on the website, you will need to reactivate the environment with:

    source env/bin/activate

Once the `virtualenv` environment is activated and the project dependencies have been installed, you can start the built-in development server with:

    mkdocs serve

## Available extensions

The website has a number of useful Markdown extensions enabled:

* [Admonition](https://squidfunk.github.io/mkdocs-material/extensions/admonition/)
* [CodeHilite](https://squidfunk.github.io/mkdocs-material/extensions/codehilite/)

## Deployment

After a new version of the website is ready and before you commit and push, make sure you build it with the `build.sh` script inside `mkdocs`.
